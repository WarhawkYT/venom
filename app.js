const path = require('path');

require('dotenv').config({path: path.join(__dirname, '.env')});
const Venom = require(path.join(__dirname, 'Venom.js')),
    start = function () {
        new Venom(process.argv[2] ? process.env.stripetoken : process.env.venomtoken).init();
    };
    
start();