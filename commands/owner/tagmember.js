const {Command} = require('discord.js-commando'), 
  {deleteCommandMessages, stopTyping, startTyping} = require('../../components/util.js');

module.exports = class TagMemberCommand extends Command {
  constructor (client) {
    super(client, {
      name: 'tagmember',
      memberName: 'tagmember',
      group: 'owner',
      description: 'Tag a member',
      format: 'MemberID|MemberName(partial or full)',
      examples: ['tagmember Favna'],
      guildOnly: false,
      ownerOnly: true,
      args: [
        {
          key: 'member',
          prompt: 'What user would you like to snoop on?',
          type: 'member'
        }
      ]
    });
  }
  
  run (msg, {member}) {
      startTyping(msg);
      deleteCommandMessages(msg, this.client);
      msg.say(`^^^^ <@${member.id}> ^^^^`);
      
      return stopTyping(msg);
  }
};